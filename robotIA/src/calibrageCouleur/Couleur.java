package calibrageCouleur;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.Color;

public class Couleur {
	
	static EV3ColorSensor couleur = new EV3ColorSensor(LocalEV3.get().getPort("S3"));
	
	
	public static void fichier(String str) {
		try {
			File file = new File("test.txt");

			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(str);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String code="";
		
		System.out.println("Press enter to calibrate Sol...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate blanc...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate rouge...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate noir...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate jaune...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate vert...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Press enter to calibrate bleu...");
		Button.ENTER.waitForPressAndRelease();
		code =code+caputre()+"\n";
		
		System.out.println("Ok!");
		
		fichier(code);
		
	}
	
	
	public static String caputre() {
		
		float[] sample = new float[3];
		couleur.setFloodlight(Color.WHITE);
		couleur.getRGBMode().fetchSample(sample, 0);
		
		return sample[0] + " " + sample[1] + " " + sample[2];
	}

}
