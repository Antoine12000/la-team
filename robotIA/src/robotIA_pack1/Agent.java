package robotIA_pack1;



import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port; 
import lejos.utility.Delay;

public class Agent {
	int part;
	Perception P;
	Actionneurs Ac;

	Agent() {
		Port PTactile = LocalEV3.get().getPort("S1");
		Port PCouleur = LocalEV3.get().getPort("S3");
		Port PUltrasons = LocalEV3.get().getPort("S2");

		Port PoueDroite = LocalEV3.get().getPort("D");
		Port ProueGauche = LocalEV3.get().getPort("A");
		Port Ppince = LocalEV3.get().getPort("C");

		P = new Perception(PTactile, PCouleur, PUltrasons);
		Ac = new Actionneurs(PoueDroite, ProueGauche, Ppince);

		System.out.println("Start ...");
		Button.waitForAnyEvent();

	}

	/** premier palet est une methode "statique" qui fait avancer le robot jusqu'au palet
	 * placé sur la 1ere ligne en face du robot, le ramasse, et le dépose dans la zone de but
	 * Après avoir ramasser le palet et pour allez dans la zone de but, le robot avance 
	 * tout droit avec un angle léger vers la droite ou la gauche selon la position du robot placé en face
	 */
	public void premierPalet(String S) {
		if (S.equals("droite")) {
			Ac.pilot.setLinearSpeed(70);
			Ac.allerA(35, 0);
			ramasserPalet();
			Ac.allerA(240, -60);
			Ac.ouvrirPince();
			Ac.reculerDe(10, 100); // a Tester
			Ac.fermerPince();
			Ac.pilot.setAngularSpeed(40);
			Ac.tournerGaucheDe(150, 60);
			identifierObjet();
			ramasserPalet();
			Ac.tournerGaucheDe(145, 40);
			Ac.avancerDe(45, 100);
			Ac.avancer(10);
			int c = P.getCouleur();
			while (!(c == 1)) {
				c = P.getCouleur();
				System.out.println(c);
				// Delay.msDelay(50);
			}
			Ac.arreter();
			Ac.avancerDe(10, 100);
			Ac.ouvrirPince();
			Ac.reculer(10); // a Tester
			int coul2 = P.getCouleur();
			while (coul2 != 3) {
				coul2 = P.getCouleur();
				System.out.println(coul2);
			}
			Ac.arreter();

			Ac.fermerPince();
			parallele();
		}

		if (S.equals("gauche")) {
			Ac.pilot.setLinearSpeed(70);
			Ac.allerA(35, 0);
			ramasserPalet();
			Ac.allerA(235, 50);
			Ac.ouvrirPince();
			Ac.reculerDe(10, 100); // a Tester
			Ac.fermerPince();
			Ac.pilot.setAngularSpeed(40);
			Ac.allerA(225, 60);
			ramasserPalet();
			Ac.tournerDroiteDe(142, 40);
			Ac.avancerDe(45, 100);
			Ac.avancer(10);
			int c = P.getCouleur();
			while (!(c == 1)) {
				c = P.getCouleur();
				System.out.println(c);
				// Delay.msDelay(50);
			}
			Ac.arreter();
			Ac.avancerDe(10, 100);
			Ac.ouvrirPince();
			Ac.reculer(10); // a Tester
			int coul2 = P.getCouleur();
			while (coul2 != 3) {
				coul2 = P.getCouleur();
				System.out.println(coul2);
			}
			Ac.arreter();

			Ac.fermerPince();
			paralleleG();
		}
			/*
			 * Ac.ouvrirPince(); Ac.reculerDe(10,100); //a Tester Ac.fermerPince();
			 * Ac.allerA(225,0); ramasserPalet(); Ac.allerA(240,0); Ac.ouvrirPince();
			 * Ac.reculerDe(10,100); //a Tester Ac.fermerPince(); Ac.allerA(240,0,180);
			 */
			if (S.equals("tm")) {
				Ac.pilot.setLinearSpeed(40);
				Ac.allerA(35, 60);
				ramasserPalet();
				Ac.allerA(240, 00);
				Ac.ouvrirPince();
				Ac.reculerDe(10, 100); // a Tester
				Ac.fermerPince();
				Ac.reculer(15);
				int c1 = P.getCouleur();
				while (!(c1 == 1)) {
					c1 = P.getCouleur();
					System.out.println(c1);
					// Delay.msDelay(50);
				}
				Ac.arreter();
				parallele(1);
				Ac.reculer(10); // a Tester
				int coul21 = P.getCouleur();
				while (coul21 != 3) {
					coul21 = P.getCouleur();
					System.out.println(coul21);
				}
				Ac.arreter();
				Ac.fermerPince();
				parallele(1);
				Ac.avancerDe((P.getDistance() + 0.14 - 1.25) * 100, 45);
				Ac.tournerDroiteDe(20, 50);
				allerLigne(3);
				parallele(3);
			}
		}
<<<<<<< HEAD
	
=======
	}
	
	/*chercherObjet() est une méthode conçue comme un sonar : le robot cherche la présence d'objet en pivotant*/
>>>>>>> e7acf89ed44f6a9c4905b7da0063b1942bb13f42

	public String chercherobjet() { // je pars du principe que le robot vient de deposer la palet dans le but
		double fff = P.getDistance();
		System.out.println("                          " + fff);
		double dist = 0.7;
		for (int i = 0; i < 3; i++) {
			Ac.tournerDroiteDe(80, 20, true);
			while (P.getDistance() > dist) {
				if (Ac.pilot.isMoving() == false) {
					break;
				}

			}
			Ac.arreter();
			if (P.getDistance() < dist) {
				return ("trouve");
			}
			Ac.tournerGaucheDe(80, 70);
			Ac.tournerGaucheDe(80, 20, true);
			while (P.getDistance() > dist) {
				if (Ac.pilot.isMoving() == false) {
					break;
				}

			}

			Ac.arreter();
			if (P.getDistance() < dist) {
				return ("trouve");
			}
			Ac.tournerDroiteDe(80, 70);
			if (i<=1) {
				Ac.avancerDe(70, 50);
			}
			part = +1;
		}
		return ("rien trouve");
	}

	/**
	 * identifierObjet() retourne le type d'objet percu grâce au capteur ultrasons :
	 * si le robot percoit encore un objet à une distance définie alors il renvoie le type d'objet
	 */
	public String identifierObjet() {
		int i = 0;
		while (0.33 < P.getDistance() && P.getDistance() < 0.7 && !Button.ESCAPE.isDown()) { // a la place du 0.1 il
			// faut mettre la mesure
			// du seuill
			double db = P.getDistance();
			// System.out.println(" "+db);
			if (i == 0) {
				Delay.msDelay(800);
				double T = P.getDistance();
				if (Math.abs(db - T) > 0.05)
					return ("Robot");
			}
			i = 1;
			Ac.tournerDroiteDe(80, 20, true);
			while (Math.abs(P.getDistance() - db) <= 0.05) {
				if (Ac.pilot.isMoving() == false) {
					break;
				}
			}
			Ac.arreter();
			Ac.tournerGaucheDe(25, 30);
			double t = P.getDistance();
			System.out.println("                                        " + t);
			Ac.avancerDe((t - 0.28) * 100, 27);
		}

		double k = P.getDistance();
		System.out.println("                                        " + k);
		if (P.getDistance() < 0.30 || P.getDistance() > 5)
			return ("mur");
		if (P.getDistance() >= 0.30) // a la place du 0.5 il faut mettre la mesure du seuill !!!
			return ("Palet");
		System.out.println("                            id   " + P.getDistance());

		return "tkt";
	}

	/**
	 * faceaumur() met le robot perpendiculaire au mur
	 * fonctionne uniquement si le robot est proche d'un mur
	 */
	public void faceaumur() {
		double db = P.getDistance();
		Ac.tournerDroiteDe(10, 50);
		double T = P.getDistance();

		if (T < db) {
			Ac.tournerD(27);
			while (T < db && !Button.ESCAPE.isDown()) {
				db = P.getDistance();
				Delay.msDelay(500);
				T = P.getDistance();
			}
			if (!Button.ESCAPE.isDown()) {
				System.exit(0);
			}
			Ac.tournerDroiteDe(25, 50);
		}
		if (T > db) {
			Ac.tournerG(27);
			while (T > db && !Button.ESCAPE.isDown()) {
				db = P.getDistance();
				Delay.msDelay(500);
				T = P.getDistance();
			}
			if (!Button.ESCAPE.isDown()) {
				System.exit(0);
			}
			Ac.tournerGaucheDe(25, 50);
		}
	}

	/**
	 * avance doucement et ferme les pinces
	 * 
	 * @return si un palet est dans la pince
	 */

	public boolean ramasserPalet() {
		Ac.ouvrirPince();
		Ac.avancer(27);
		int comp = 0;
		while (!P.paletDansPinces() && !Button.ESCAPE.isDown() && comp < 60000) {
			comp++;
		}
		Ac.arreter();
		Ac.fermerPince();
		return (comp < 20000);
	}

	/** 
	 * le robot se déplace jusqu'à se trouver sur la ligne de couleur indiqué en paramètre où il s'y arrete
	 */
	public boolean allerLigne(int coul) {
		int c = P.getCouleur();
		if (!(c == coul)) {
			Ac.avancer(20);
			while (!(c == coul)) {
				c = P.getCouleur();
				System.out.println(c);
				// Delay.msDelay(50);
			}
			Ac.arreter();
		}
		return (c == coul);

	}


	/** 
	 * place le robot dans l'alignement de la ligne sur laquelle il se trouve (en tournant vers la droite), 
	 * et quelque soit la couleur de la ligne
	 */
	public int parallele() { 
		int couleur = P.getCouleur();
		Ac.avancerDe(10, 10);
		int coul2 = P.getCouleur();
		Ac.tournerD(28);
		while (coul2 != couleur) {
			coul2 = P.getCouleur();
			System.out.println(coul2);

		}
		Ac.arreter();
		return coul2;
	}

	/**
	 * place le robot dans l'alignement de la ligne sur laquelle il se trouve (en tournant vers la droite), 
	 * en prenant en paramètre la couleur de la ligne en question
	 */
	public int parallele(int coul) { 

		Ac.avancerDe(10, 10);
		int coul2 = P.getCouleur();
		Ac.tournerD(28);
		while (coul2 != coul) {
			coul2 = P.getCouleur();
			System.out.println(coul2);

		}
		Ac.arreter();
		return coul2;
	}

	/** 
	 * place le robot dans l'alignement de la ligne sur laquelle il se trouve (en tournant vers la gauche), 
	 * et quelque soit la couleur de la ligne
	 */
	public int paralleleG() { 
		int couleur = P.getCouleur();
		Ac.avancerDe(10, 10);
		int coul2 = P.getCouleur();
		Ac.tournerG(28);
		while (coul2 != couleur) {
			coul2 = P.getCouleur();
			System.out.println(coul2);

		}
		Ac.arreter();
		return coul2;
	}

	/** 
	 * le robot va dans la zone de but en tournant d'un angle défini à partir 
	 * de ses précédents déplacements puis en avancant tout droit
	 */
	public boolean allerDansButs() {
		Ac.pilot.setAngularSpeed(27);
		Ac.navig.rotateTo(0);
		if (part == 0) {
			allerbut();
		}
		if (part == 1) {	
			Ac.avancerDe(70, 100);
			allerbut();
		}
		return false;
	}

	/**
	 * une fois que le robot est dans la zone de but il avance 
	 * légèrement pour etre sur de déposer le palet dans la zone et dépose le palet; 
	 * puis s'aligne sur la ligne blanche
	 */
	public boolean allerbut() {
		allerLigne(1);
		Ac.avancerDe(15, 30);
		Ac.ouvrirPince();
		int c = P.getCouleur();
		Ac.reculer(6);
		while (!(c == 1)) {
			c = P.getCouleur();
			System.out.println(c);
			// Delay.msDelay(50);
		}
		Ac.arreter();
		Ac.fermerPince();
		parallele();
		return true;
	}

	/** 
	 * lance la methode premier palet, puis la methode IA juqu'à l'appuie du bouton d'arret;
	 * en fonction du coté où part le robot
	 */
	public void start(String s) {
		premierPalet("droite");
		while (!Button.ESCAPE.isDown()) {
			ia();
		}
		/*chercherobjet();
		identifierObjet();
		ramasserPalet();*/
		}



	/** 
	 * méthode pour ramasser les autres palets (le premier ayant déjà été ramassé
	 * principe : le robot cherche un objet, une fois trouvé, il l'identifié, si c'est un palet
	 * il le ramasse et l'amène dans la zone de but, sinon il continue de chercher jusqu'à trouver un palet
	 */
	public boolean ia() {
		String cherobj = chercherobjet();
		System.out.println("                " + cherobj);
		if (cherobj == "trouve") {
			String idObj = identifierObjet();
			System.out.println("                " + idObj);
			if (idObj == "Palet") {
				ramasserPalet();
				System.out.println("             OK rp");
				allerDansButs();
				System.out.println("                            " + P.getDistance());
				Ac.avancerDe((P.getDistance() + 0.14 - 1.25) * 100, 45);
				Ac.tournerDroiteDe(20, 50);
				allerLigne(3);
				parallele(3);
				// Ac.allerA(0, 0, 0);
			}
			if (idObj == "mur") {
				Ac.avancerDe((P.getDistance() + 0.14 - 1.95) * 100, 45);
				int c = P.getCouleur();
				if (!(c == 3)) {
					Ac.reculer(10);
					while (!(c == 3)) {
						c = P.getCouleur();
						System.out.println(c);
						// Delay.msDelay(50);
					}
					Ac.arreter();
					Ac.navig.rotateTo(180);
				}

			}

			if (cherobj.equals("rien trouve")) {
				Ac.navig.rotateTo(0);
				Ac.avancerDe(200, 50);
				allerDansButs();
			}

		}
		return true;
	}

	/**
	 * methode principale qui lance le programme
	 */
	public static void main(String[] args) {
		Agent robo = new Agent();
		robo.start(""); // dans "" mettre entre "droite", "gauche", "mildroite", "milgauche"

	}

}
