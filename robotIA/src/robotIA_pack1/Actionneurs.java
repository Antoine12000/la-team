package robotIA_pack1;

import lejos.hardware.port.Port;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;

/**
 * Classe permet de controler tous les moteurs
 */
public class Actionneurs {

	RegulatedMotor motorDroit, motorGauche, motorpince;

	Wheel roueGaucheW, roueDroiteW;

	Chassis chassis;

	MovePilot pilot;
	
	Navigator navig;
	
	Path route;

	boolean pinceOuverte = false;
	
	/*
	 * memoire des angle du robot
	 */


	public Actionneurs(Port pRoueDroite, Port pRoueGauche, Port pPince) {
		motorDroit = new EV3LargeRegulatedMotor(pRoueDroite);
		motorGauche = new EV3LargeRegulatedMotor(pRoueGauche);
		motorpince = new EV3LargeRegulatedMotor(pPince);
		motorpince.setSpeed(900);

		roueGaucheW = WheeledChassis.modelWheel(motorGauche, 5.6).offset(-5.3);
		roueDroiteW = WheeledChassis.modelWheel(motorDroit, 5.6).offset(5.3);

		chassis = new WheeledChassis(new Wheel[] { roueGaucheW, roueDroiteW }, WheeledChassis.TYPE_DIFFERENTIAL);

		pilot = new MovePilot(chassis);
		navig =new Navigator(pilot);
		

	}
	
	/**
	* permet de faire avancer le robot à la vitesse s donnée en paramètre
	*/
	public void avancer(double s) {
		pilot.setLinearSpeed(s);
		pilot.travel(300,true);
	}

	/**
	* permet de faire reculer le robot à la vitesse s donnée en paramètre
	*/
	public void reculer(double s) {
		pilot.setLinearSpeed(s);
		pilot.travel(-300,true);
	}
	
	/**
	* permet d'arreter le robot
	*/
	public void arreter() {
		pilot.stop();
	}
	
	/**
	* permet de faire avancer le robot, à la vitesse s, d'une distance (en cm) donnee en parametre
	*/
	public void avancerDe(double a, double s) {
		pilot.setLinearSpeed(s);
		pilot.travel(a);

	}
	
	public void avancerDe(double a,double s,boolean t) {
		pilot.setLinearSpeed(s);
		pilot.travel(a,t);
	}
	
	/**
	* permet de faire reculer le robot, à la vitesse s, d'une distance (en cm) donnee en parametre
	*/
	public void reculerDe(double a,double s) {
		pilot.setLinearSpeed(s);
		pilot.travel(-a);
	}
	
	/*les 2 méthodes allerA permettent de faire déplacer le robot jusqu'à la destination Waypoint 
	créee à partir des paramètres*/

	public void allerA(int x,int y) {
		boolean roule = true;
		navig.goTo(x, y);
		while(roule) {
			roule = navig.isMoving();
			if(Button.ESCAPE.isDown()) {		
				System.exit(0);
			}
		}
	}
	
	public void allerA(int x,int y,int angle) {
		boolean roule = true;
		navig.goTo(x, y,angle);
		while(roule) {
			roule = navig.isMoving();
			if(Button.ESCAPE.isDown()) {		
				System.exit(0);
			}
		}
	}
	
	
	//Tourner
	
	/**
	* permet de faire tourner le robot de 360 degrés vers la droite, à la vitesse s donnée en paramètre
	*/
	public void tournerD(double s) {
		pilot.setAngularSpeed(s);
		pilot.rotate(360, true);
	}
	
	/**
	* permet de faire tourner le robot de 360 degrés vers la gauche, à la vitesse s donnée en paramètre
	*/
	public void tournerG(double s) {	
		pilot.setAngularSpeed(s);
		pilot.rotate(-360, true);
	}

	
	/**
	* permet de faire tourner le robot vers la droite, d'un angle donnée d, à la vitesse s
	*/
	public void tournerDroiteDe(int d,double s) {
		pilot.setAngularSpeed(s);
		pilot.rotate(d);
		
	}
	
	public void tournerDroiteDe(int d,double s, boolean t) {
		pilot.setAngularSpeed(s);
		pilot.rotate(d,t);
	}
	
	/**
	* permet de faire tourner le robot vers la gauche, d'un angle donnée d, à la vitesse s
	*/
	public void tournerGaucheDe(int d,double s) {
		pilot.setAngularSpeed(s);
		pilot.rotate(-d);

	}
	
	public void tournerGaucheDe(int d,double s, boolean t) {
		pilot.setAngularSpeed(s);
		pilot.rotate(-d, t);
	}
		

	/**
	* permet au robot de faire un demi tour
	*/
	public void demitour(double s) {
		pilot.setLinearSpeed(s);
		tournerDroiteDe(180, s);
	}
	

	
	
	//____________________pince_______________________

	/**
	* permet d'ouvrir les pinces du robot
	*/
	public boolean ouvrirPince() {
		if (!pinceOuverte) {
			motorpince.forward();
			Delay.msDelay(2500);
			motorpince.stop();
			pinceOuverte = true;
			return true;
		} else
			return false;
	}

	/**
	* permet de fermer les pinces du robot
	*/
	public boolean fermerPince() {
		if (pinceOuverte) {
			motorpince.backward();
			Delay.msDelay(2500);
			motorpince.stop();
			pinceOuverte = false;
			return true;
		}else 
			return false;
	}

}
