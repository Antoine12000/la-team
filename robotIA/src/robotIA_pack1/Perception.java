package robotIA_pack1;

import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3ColorSensor;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import lejos.hardware.sensor.EV3UltrasonicSensor;

public class Perception {

	EV3TouchSensor barre;
	EV3ColorSensor couleur;
	EV3UltrasonicSensor ultrasons;

	double valCouleur[][] = lire();

	private double[][] lire() {
		double valeur[][] = new double[7][3];
		int i = 0;
		try {
			BufferedReader in = new BufferedReader(new FileReader("test.txt"));
			String line;
			while ((line = in.readLine()) != null) {
				String mots[] = line.split(" ");
				for (int j = 0; j < 3; j++) {
					valeur[i][j] = Double.valueOf(mots[j]).doubleValue();
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return valeur;
	}

	public Perception(Port capteurTactile, Port capteurCouleur, Port capteurUltrasons) {
		this.barre = new EV3TouchSensor(capteurTactile);
		this.couleur = new EV3ColorSensor(capteurCouleur);
		this.ultrasons = new EV3UltrasonicSensor(capteurUltrasons);
	}

	public boolean paletDansPinces() {
		float[] sample = new float[1];
		barre.getTouchMode().fetchSample(sample, 0);
		return sample[0] != 0;
	}

	public double getDistance() {
		SampleProvider d = ultrasons.getDistanceMode();
		float sample[] = new float[1];
		d.fetchSample(sample, 0);
		return sample[0];
	}

	public int getCouleur() {
		double min = Double.MAX_VALUE;
		int col=-1;
		double scalaire;
		couleur.setFloodlight(Color.WHITE); //?
		float[] sample = new float[3];
		couleur.getRGBMode().fetchSample(sample, 0);
		for (int i = 0; i < 7; i++) {
			scalaire=scalaire(sample, valCouleur[i]);
			if (scalaire < min) {
				min=scalaire;
				col= i;
			}
		}
		return col;
	}

	private static double scalaire(float[] v1, double[] v2) {
		return Math.sqrt(Math.pow(v1[0] - v2[0], 2.0) + Math.pow(v1[1] - v2[1], 2.0) + Math.pow(v1[2] - v2[2], 2.0));
	}


	public static void main(String[] args) {

	}

}
